import wikipedia
import urllib.request
import re
from urllib.parse import unquote
import os
import glob

def removeFiles(path):
    files = glob.glob(path + '/*')
    for f in files:
        os.remove(f)

page = wikipedia.page(input('Enter your searchprompt: '))
print(page.summary)

removeFiles(os.path.abspath(os.getcwd()) + '/wiki')

workingHtml = unquote(page.html())
imgSrcInHtmlArr = re.findall('src=\"(.*?)\"', unquote(str(page.html())))

for remoteImageName in page.images:
    localImageName = unquote(str(remoteImageName).split('/')[-1])

    for imgSrc in imgSrcInHtmlArr:
            if localImageName in imgSrc:
                workingHtml = str(workingHtml).replace(imgSrc, 'wiki/' + localImageName)

    print(remoteImageName)
    urllib.request.urlretrieve(remoteImageName, 'wiki/' + localImageName)

srcsetLocation = re.findall('srcset=\"(.*?)\"', workingHtml)
for src in srcsetLocation:
    workingHtml = unquote(str(workingHtml).replace(src, ""))

headConfig = open('config/wikiHead.html').read()

file = open("wiki.html", "w")
file.write(unquote(headConfig))
file.write(unquote(workingHtml))
file.close()
